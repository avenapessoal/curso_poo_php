<?php

require_once 'Pessoa.php';
// não permite que classe alune seja herdada, ela a final/folha
//final class Aluno extends Pessoa {
class Aluno extends Pessoa {
    private $matr;
    private $curso;

    //com a palavra final nao permite o metodo ser sobreposto
    //public final function pagarMensalidade() {
        
    public function pagarMensalidade() {
        echo "<p>Pagando mensalidade do aluno $this->nome </p> <br/>";
    }

    public function getMatr() {
        return $this->matr;
    }
    public function setMatr($matr) {
        $this->matr = $matr;
    }

    public function getCurso() {
        return $this->curso;
    }
    public function setCurso($curso) {
        $this->curso = $curso;
    }

}

?>