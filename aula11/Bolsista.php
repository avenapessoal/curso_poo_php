<?php

require_once 'Aluno.php';

class Bolsista extends Aluno {
    private $bolsa;

    public function renovarBolsa() {
        echo "<p>Bolsa renovada</p>";
    }

    // sobre-escrevendo o metodo de - pagarMensalidade()  de aluno.
    public function pagarMensalidade() {
        echo "<p>$this->nome é um bolsista. Pagando com desconto para aluno $this->nome </p>";
    }

    public function getBolsa() {
        return $this->bolsa;
    }
    public function setBolsa($bolsa) {
        $this->bolsa = $bolsa;
    }


    
}