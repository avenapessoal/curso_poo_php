<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Curso POO PHP – 11b – Herança (Parte 2)</title>
</head>
<body>
<pre>

<?php

    require_once 'Visitante.php';
    require_once 'Aluno.php';
    require_once 'Bolsista.php';
    

    $v1 = new Visitante();
    $v1->setNome("Juvenal");
    $v1->setIdade(32);
    $v1->setSexo("M");


    $a1 = new Aluno();
    $a1->setNome("Pedro");
    $a1->setMatr(1111);
    $a1->setIdade(16);
    $a1->setSexo("M");
    $a1->setCurso("Informatica");
    $a1->pagarMensalidade();


    print_r($v1);

    print_r($a1);

    $b1 = new Bolsista();
    $b1->setNome("Jorge Bolsista");
    $b1->setMatr(54115);
    $b1->setIdade(16);
    $b1->setSexo("M");
    $b1->setCurso("Informatica");
    $b1->setBolsa(12.5);
    $b1->renovarBolsa();
    $b1->pagarMensalidade();

    print_r($b1);
    
    
    
?>

</pre>
</body>
</html>