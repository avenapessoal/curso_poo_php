<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Curso POO PHP – 14b – Projeto Final em PHP (Parte 1)</title>
</head>
<body>

<pre>
<?php

require_once 'Gafanhoto.php';
require_once 'Video.php';

$v[0] = new Video("Aula 1 de POO");
$v[1] = new Video("Aula 12 de PHP");
$v[2] = new Video("Aula 15 de HTML5");


$v[0]->play();


$g = new Gafanhoto("Pedro",12,"M","pedrinho");
$g->viuMaisUm();
print_r($v);
echo "<hr>";
print_r($g);


?>
</pre>
    
</body>
</html>