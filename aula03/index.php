<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Aula 03 - POO</title>
</head>
<body>
    
<?php
require_once 'Caneta.php';

$c1 = new Caneta;
$c1->modelo = "Bic Cristal";
$c1->cor = "Azul";
//$c1->ponta = 0.5;
$c1->destampar();

print_r($c1);
echo "<br/><br/>";

$c1->rabiscar();

$c2 = new Caneta;
$c2->cor = "Verde";
//$c2->carga = 50;
$c2->tampar();

echo "<br/><br/>";
print_r($c2);

echo "<br/><br/>";
$c2->rabiscar();


?>
</body>
</html>