<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Curso POO PHP – 14b – Projeto Final em PHP (Parte 1)</title>
</head>
<body>

<pre>
<?php

require_once 'Gafanhoto.php';
require_once 'Video.php';
require_once 'Visualizacao.php';

$v[0] = new Video("Aula 1 de POO");
$v[1] = new Video("Aula 12 de PHP");
$v[2] = new Video("Aula 15 de HTML5");


$v[0]->play();



print_r($v);
echo "<hr>";

$g[0] = new Gafanhoto("Pedro",12,"M","pedrinho");
$g[1] = new Gafanhoto("Creuza",23,"F","creuza");


print_r($g);

echo "<hr>";

$vis[0] = new Visualizacao($g[0],$v[2]);
$vis[1] = new Visualizacao($g[0],$v[1]);
$vis[1]->avaliar_porc(85);  // tem colocar aqui a nota 8+1 = 9 
                            // pois se colocar depois em <h1>avaliacao</h1>
                            // la o 9 vai estar sendo dividido por 2

$vis[2] = new Visualizacao($g[1],$v[1]);
$vis[2]->avaliar_nota(5);

print_r($vis);

echo "<hr>";
echo "<h1>Avaliando</h1>";
$vis[0]->avaliar();
$vis[1]->avaliar_porc(85);
$vis[2]->avaliar_nota(5);
//$vis[1]->

print_r($vis);
?>
</pre>
    
</body>
</html>