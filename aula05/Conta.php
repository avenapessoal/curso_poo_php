<?php
// criação da classe
class ContaBanco{
    public $numConta;
    protected $tipo;
    private $dono;
    private $saldo;
    private $status;


    public function __construct() {
        $this->setStatus(false);
        $this->setSaldo(0);
        echo "<p>Conta criada com sucesso!</p>";

    }

    public function abrirConta($t) {
        $this->setTipo($t);
        $this->setStatus(true);
        if($t == "CC"){
            $this->setSaldo(50);
        }
        elseif ($t == "CP") {
            $this->setSaldo(150);
        }
    }


    public function fecharConta() {
        if($this->getSaldo() > 0) {
            echo("Conta com dinheiro. Não posso fechar!!");
        }
        elseif($this->getSaldo() < 0 ){
            echo("Conta com debito! Não posso fechar!!");
        }
        else{
            $this->setStatus(false);
            echo("Conta de " . $this->getDono() . " fechada!!!!");
        }
    }


    public function depositar($v) {
        if($this->getStatus()){
           // $this->saldo += $v;
           $this->setSaldo($this->getSaldo() + $v);
           echo "</p>Deposito de $v na conta de " . $this->getDono() . " </p>";
        }
        else {
           echo("<p>Impossivel depositar</p>");
        }
    }


    public function sacar($v) {
        if($this->getStatus()) {
            if($this->getSaldo() >= $v) {
                $this->setSaldo($this->getSaldo() - $v);
                echo("<p>Saque de R$ $v autorizado na conta de " . $this->getDono() . " </p>");
            }
            else{
                echo("<p>Saldo insuficiente!<p>");            
            }
        }
        else {
            echo("<p>Impossivel sacar</p>");
        }
    }

    public function pagarMensal() {
        //$v=0;
        if($this->getTipo() == "CC"){
            $v = 12;
        }
        elseif ($this->getTipo() == "CP"){
            $v = 20;
        }
        if($this->getStatus()){
            if($this->getSaldo() > $v ) {
                $this->setSaldo($this->getSaldo() - $v);
                echo("<p>Mensalidade de R$ $v debitada na conta de " . $this->getDono() . " </p>");
            }
            else {
                echo("Saldo insuficiente");
            }
        }
        else {
            echo("Impossivel pagar");
        }
    }

    public function getnumConta($numConta){
        return $this->numConta;
    }

    public function setnumConta($numConta){
        $this->numConta = $numConta;
    }

    public function getTipo(){
        return $this->tipo;
    }

    public function setTipo($tipo){
        $this->tipo = $tipo;
    }
    
    public function getDono(){
        return $this->dono;
    }

    public function setDono($dono){
        $this->dono = $dono;
    }
    
    public function getSaldo(){
        return $this->saldo;
    }

    public function setSaldo($saldo){
        $this->saldo = $saldo;
    }
    
    public function getStatus(){
        return $this->status;
    }

    public function setStatus($status){
        $this->status = $status;
    }
    
}



?>