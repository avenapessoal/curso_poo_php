<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Aula 04 - POO - Métodos Especiais - Getter, Setter e Construtor</title>
</head>
<body>
 <pre>   
<?php
require_once 'Conta.php';

$p1 = new ContaBanco();
$p2 = new ContaBanco();

$p1->setDono("Jubileu");
echo "<h1>Movimentação de " .$p1->getDono(). "</h1>";
$p1->abrirConta("CC");
$p1->setnumConta(1111);
$p1->depositar(300);
$p1->pagarMensal();
$p1->sacar(330);
$p1->fecharConta();


$p2->abrirConta("CP");
$p2->setDono("Creusa");
echo "<h1>Movimentação de " .$p2->getDono(). "</h1>";
$p2->setnumConta(2222);
$p2->depositar(500);
$p2->sacar(100);
$p2->pagarMensal();
$p2->sacar(530);
$p2->fecharConta();

echo "<br /><br />";
print_r($p1);
echo "<br />";
print_r($p2);
?>

</pre>
</body>
</html>