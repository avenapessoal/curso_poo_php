<?php
// criação da classe
class Caneta{
    public $modelo;
    public $cor;
    private $ponta;
    protected $carga;
    protected $tampada;

    //public function Caneta(){ // pode ter o mesmo nome da Classe
    public function __construct($m,$c,$p) {
        $this->setModelo($m);
        $this->cor = $c;
        $this->setPonta($p);
        $this->setTampar();

    }

    public function setTampar() {
        $this->tampada = true;
    }

    public function getModelo() {
        return $this->modelo;
    }

    public function setModelo($m) {
        $this->modelo = $m;
    }

    public function getPonta() {
        return $this->ponta;
    }

    public function setPonta($p) {
        $this->ponta = $p;
    }
}



?>