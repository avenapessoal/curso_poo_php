<?php

require_once 'Lobo.php';

class Cachorro extends Lobo {
    protected $corPelo;
    public $frase;
    public $hora;
    public $min;
    public $dono;

    public function emitirSom() {
        echo "<p>Au!Au!Au!Au!</p>";
    }
    
    // SOBRECARGA
    public function reagirFrase($frase){
        echo "<p>Reagindo a Frase $this->frase = $frase </p>";
    }

    public function reagirHora($hora,$min){
        echo "<p>Reagindo a Hora $this->hora = $hora e Minuto $this->min = $min </p>";
    }

    public function reagirDono($dono) {
        echo "<p>Reagindo ao Dono $this->dono = $dono</p>";
    }

    public function reagirIdadePeso($idade,$peso) {
        echo "<p>Reagindo a idade $this->idade = $idade e peso $this->peso = $peso</p>";
    }
    
}

?>