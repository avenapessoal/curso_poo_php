<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Curso POO Teoria – 12a – Conceito Polimorfismo (Parte 1)</title>
</head>
<body>

<pre>

<?php

require_once 'Mamifero.php';
require_once 'Lobo.php';
require_once 'Cachorro.php';

$m = new Mamifero();
$l = new Lobo();
$c = new Cachorro();

// polimorfismo sobreposição - chamando mesmo metodo locomover() mas vem outro texto para animal

$m->emitirSom();

echo "<hr>";

$l->emitirSom();

echo "<hr>";

$c->emitirSom();

echo "<hr>";


print_r($m);
print_r($l);
print_r($c);


echo "<hr>";
echo "<h1>Sobrecarga</h1>";
echo "<hr>";

$c->reagirFrase("Bom dia lindo");
$c->reagirHora(11,50);
$c->reagirDono("Ana Morena");
$c->reagirIdadePeso(32,80);


?>



</pre>    
</body>
</html>

