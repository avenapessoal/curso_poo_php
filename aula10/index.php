<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Curso POO Teoria – 10 – Herança</title>
</head>
<body>
 <pre>   
<?php

require_once 'Pessoa.php';
require_once 'Aluno.php';
require_once 'Professor.php';
require_once 'Funcionario.php';

/*
$p1 = new Pessoa("h",32,"f");
$p2 = new Aluno("h",32,"f");
$p3 = new Professor("h",32,"f");
$p4 = new Funcionario("h",32,"f");
*/

$p1 = new Pessoa();
$p2 = new Aluno();
$p3 = new Professor();
$p4 = new Funcionario();

$p1->setNome("Pedro");
$p2->setNome("Maria");
$p3->setNome("Claudio");
$p4->setNome("Fabiana");

$p2->setMatr("25647");
$p2->setCurso("Informática");
$p2->setSexo("F");

$p3->setSalario("2500.75");
$p4->setSetor("Estoque");


$p2->setIdade(14);
$p2->fazerAniver();
$p3->receberAum(3000);

// codigo daria erro...pois usando metodos que a classe nao tem

// aluno nao pode mudar de trabalho
//$p2->mudarTrabalho();
// mas funcionario pode mudar de trabalho
$p4->mudarTrabalho();

print_r($p1);
print_r($p2);
print_r($p3);
print_r($p4);
?>

</pre>
</body>
</html>