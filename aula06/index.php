<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Aula 06 - POO - Encapsulamento</title>
</head>
<body>
 <pre>   

<h1>Projeto Controle Remoto</h1>
<?php
require_once 'ControleRemoto.php';

$c = new ControleRemoto();
$c->abrirMenu();
$c->ligar();
$c->abrirMenu();

$c->maisVolume();
$c->desligar();
$c->abrirMenu();

$c->maisVolume();
$c->abrirMenu();

$c->ligar();
$c->play();
$c->abrirMenu();


?>

</pre>
</body>
</html>