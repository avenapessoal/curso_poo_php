<?php

abstract class Animal {
    protected $peso;
    protected $idade;
    protected $membros;

    abstract public function alimentar();
    abstract public function emitirSom();
    abstract public function locomover();

    public function setPeso($peso) {
        $this->peso = $peso;
    }
    public function getPeso() {
        return $this->peso;
    }

    public function setIdade($idade) {
        $this->idade = $idade;
    }
    public function getIdade() {
        return $this->idade;
    }

    public function setMembros($membros) {
        $this->membros = $membros;
    }
    public function getMembros() {
        return $this->membros;
    }

}


?>