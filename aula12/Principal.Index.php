<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Curso POO Teoria – 12a – Conceito Polimorfismo (Parte 1)</title>
</head>
<body>

<pre>

<?php

require_once 'Mamifero.php';
require_once 'Peixe.php';
require_once 'Reptil.php';
require_once 'Ave.php';
require_once 'Canguru.php';
require_once 'Cachorro.php';

$m = new Mamifero();
$r = new Reptil();
$p = new Peixe();
$a = new Ave();

// polimorfismo sobreposição - chamando mesmo metodo locomover() mas vem outro texto para animal
$m->setPeso(85.3);
$m->setIdade(2);
$m->setMembros(4);
$m->setcorPelo("Preto");
$m->locomover();
$m->alimentar();
$m->emitirSom();

echo "<hr>";

$p->setPeso(0.35);
$p->setIdade(1);
$p->setMembros(0);
$p->setcorEscama("Brilhante");
$p->locomover();
$p->alimentar();
$p->emitirSom();
$p->soltarBolha();

echo "<hr>";

$a->setPeso(0.89);
$a->setIdade(2);
$a->setMembros(2);
$a->setcorPena("Azul");
$a->locomover();
$a->alimentar();
$a->emitirSom();
$a->fazerNinho();

echo "<hr>";

$r->setPeso(0.89);
$r->setIdade(8);
$r->setMembros(4);
$r->setcorEscama("Marrom");
$r->locomover();
$r->alimentar();
$r->emitirSom();


echo "<hr>";

print_r($m);
print_r($r);
print_r($p);
print_r($a);

echo "<hr>";

$m2 = new Mamifero();
$m2->setPeso(5.70);
$m2->setIdade(8);
$m2->setMembros(4);
$m2->locomover();
$m2->alimentar();
$m2->emitirSom();


echo "<hr>";

$c = new Canguru();
$c->setPeso(55.30);
$c->setIdade(3);
$c->setMembros(4);
$c->locomover();
$c->alimentar();
$c->emitirSom();
$c->usarBolsa();

echo "<hr>";

$k = new Cachorro();
$k->setPeso(55.30);
$k->setIdade(3);
$k->setMembros(4);
$k->locomover();
$k->alimentar();
$k->emitirSom();
$k->enterrarOsso();
$k->abanarRabo();


?>



</pre>    
</body>
</html>

