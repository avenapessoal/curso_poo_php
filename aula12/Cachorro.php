<?php

require_once 'Mamifero.php';

class Cachorro extends Mamifero {
    
    public function enterrarOsso() {
        echo "<p>Cachorro enterrando seu osso</p>";
    }

    public function abanarRabo() {
        echo "<p>Cachorro abanando o rabo</p>";
    }
}

?>