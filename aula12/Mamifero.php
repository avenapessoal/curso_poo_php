<?php

require_once 'Animal.php';

class Mamifero extends Animal {
    private $corPelo;

    public function locomover(){
        echo "<p>Mamifero Correndo</p>";
    }
     public function alimentar(){
         echo "<p>Mamando</p>";
                
    }
    public function emitirSom(){
        echo "<p>Som de Mamifero</p>";       
    }
                
    public function setcorPelo($corPelo) {
        $this->corPelo = $corPelo;
    }
    public function getcorPelo() {
        return $this->corPelo;
    }

}

?>