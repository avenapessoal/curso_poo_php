<?php

require_once 'Animal.php';

class Ave extends Animal {
    private $corPena;


    function fazerNinho() {
        echo "<p>Ave fazendo Ninho</p>";
    }

    public function locomover(){
        echo "<p>Ave Voando</p>";
    }
     public function alimentar(){
         echo "<p>Comendo frutas</p>";
                
    }
    public function emitirSom(){
        echo "<p>Som da Ave</p>";       
    }
               
    public function setcorPena($corPena) {
        $this->corPena = $corPena;
    }
    public function getcorPena() {
        return $this->corPena;
    }



}

?>