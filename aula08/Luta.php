<?php

require_once 'Lutador.php';

class Luta {
    private $desafiado;
    private $desafiante;
    private $rounds;
    private $aprovada;
    

    public function marcarLuta($l1,$l2) {
        if(($l1->getCategoria() === $l2->getCategoria()) && ($l1 != $l2) ) {
            $this->aprovada = true;
            $this->desafiado = $l1;
            $this->desafiante = $l2;
        }
        else{
            $this->aprovada = false;
            $this->desafiado = null;
            $this->desafiante = null;
        }
    }

    public function lutar() {
        if($this->aprovada) {
            $this->desafiado->apresentar();
            $this->desafiante->apresentar();
            $vencedor = rand(0,2);
            switch($vencedor){
                case 0: // empate
                    echo "Empate!!";
                    $this->desafiado->empatarLuta();
                    $this->desafiante->empatarLuta();
                    break;
                case 1: // Desafiado vence
                    echo "Venceu o desafiado: " . $this->desafiado->getNome() . "</p>";  
                    $this->desafiado->ganharLuta();
                    $this->desafiante->perderLuta();
                    break;
                case 2: // Desafiante vence
                    echo "<p>Venceu o desafiante: " . $this->desafiante->getNome() . "</p>";                    
                    $this->desafiado->perderLuta();
                    $this->desafiante->ganharLuta();
                    break;
            }
        }
        else {
            echo "<p>Luta não pode acontecer</p>";
        }
    }

    // metodos especiais
    public function setDesafiado($desafiado) {
        $this->desafiado = $desafiado;
    }
    public function getDesafiado() {
        return $this->desafiado;
    }

    private function setDesafiante($desafiante) {
        $this->desafiante = $desafiante;
    }
    private function getDesafiante() {
        return $this->desafiante;
    }

    private function setRounds($rounds) {
        $this->rounds = $rounds;
    }
    private function getRounds() {
        return $this->rounds;
    }

    private function setAprovada($aprovada){
        $this->aprovada = $aprovada;
    }
    private function getAprovada(){
        return $this->aprovada;
    }

}

?>