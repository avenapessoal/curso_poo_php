<?php
class Lutador {
    private $nome;
    private $nacionalidade;
    private $idade;
    private $altura;
    private $peso;
    private $categoria;
    private $vitorias;
    private $derrotas;
    private $empates;

    public  function __construct($no,$na,$id,$al,$pe,$vi,$de,$em){
        $this->nome = $no;
        $this->nacionalidade = $na;
        $this->idade = $id;
        $this->altura = $al;
        $this->setPeso($pe);
        $this->vitorias = $vi;
        $this->derrotas = $de;
        $this->empates = $em;
    }

    public function apresentar(){
        echo "<p>--------------------------------------------</p>";
        echo "<p>---------------Apresentação-----------------</p>";
        echo "<p>--------------------------------------------</p>";
        echo "<p>Lutador: " . $this->getNome();
        echo " de Nascionalidade: " . $this->getNacionalidade() ."</p>";
        echo "<p>Tem " . $this->getIdade() . " anos com ";
        echo $this->getAltura() . " m de altura</p>";
        echo "Pesando: " . $this->getPeso()."<br>";
        echo "Ganhou: " . $this->getVitorias()."<br>";
        echo "Perdeu: " . $this->getDerrotas()."<br>";
        echo "Empatou: " . $this->getEmpates()."<br>";
        echo "<p>--------------------------------------------</p>";
    }

    public function status() {
        echo "</br>O lutador " . $this->getNome();
        echo " é tem um peso " . $this->getPeso();
        echo " Com ". $this->getVitorias() . ", vitórias, ";
        echo $this->getDerrotas() . " derrotas, "; 
        echo $this->getEmpates() . " empates.";

    }

    public function ganharLuta() {
        $this->setVitorias($this->getVitorias() + 1);
    }

    public function perderLuta() {
        $this->setDerrotas($this->getDerrotas() + 1);
    }

    public function empatarLuta() {
        $this->setEmpates($this->getEmpates() + 1);
    }

    private function getNome() {
        return $this->nome;
    }
    private function setNome($nome){
        $this->nome = $nome;
    }

    private function getNacionalidade(){
        return $this->nacionalidade;
    }
    private function setNacionalidade($na){
        $this->nacionalidade = $na;
    }

    private function getIdade(){
        return $this->idade;
    }
    private function setIdade($id){
        $this->idade = $id;
    }

    private function getAltura(){
        return $this->altura;
    }
    private function setAltura($al){
        $this->altura = $al;
    }

    private function getPeso(){
        return $this->peso;
    }
    private function setPeso($pe) {
        $this->peso = $pe;
        $this->setCategoria();
    }

    private function getCategoria() {
        return $this->categoria;
    }

    private function setCategoria() {
        if($this->getPeso() < 52.2) {
            $this->categoria = "Invalido";
        }
        elseif($this->getPeso() < 70.3) {
            $this->categoria = "Leve";
        }
        elseif($this->getPeso() < 83.9) {
            $this->categoria = "Medio";
        }
        elseif($this->getPeso() < 120.2){
            $this->categoria = "Pesado";
        }
        else{
            $this->categoria = "Invalido";
        }


    }

    private function getVitorias() {
        return $this->vitorias;
    }
    private function setVitorias($vi) {
        $this->vitorias = $vi;
    }

    private function getDerrotas(){
        return $this->derrotas;
    }
    private function setDerrotas($de){
        $this->derrotas = $de;
    }

    private function getEmpates(){
        return $this->empates;
    }
    private function setEmpates($em){
        $this->empates = $em;
    }
   
}